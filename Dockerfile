FROM eclipse-temurin:11-jdk-alpine

EXPOSE 8080

COPY main.jar main.jar

CMD ["java","-jar","/main.jar"]